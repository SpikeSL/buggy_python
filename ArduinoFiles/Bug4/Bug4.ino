 #include <BUG_MOTOR.h>
#include <ros.h>
#include <robot_system/Joy_2_Arduino.h>
#include <robot_system/IR_Sensors.h>

int count = 0;                    // ----> This variable will increment with via the encorder inturrpts
char STAT;                        // ----> This variable stores the State.
char* PtrState = &STAT ;          // ----> This is the pointer to the state.
int FallOut = 0;
int Speed;
int* ptrSpeed = &Speed;

ros::NodeHandle nh;               // ------> Create the Node Handler
robot_system::IR_Sensors ir_msg;

void messageCb(const robot_system::Joy_2_Arduino Bug_State)  //----------> Reading the data
{
  Speed = Bug_State.speed;
  // G = 1
  // S = 2
  // L = 3
  // R = 4
    if (Bug_State.direction == 1) 
    {
       STAT = 'G';
    }
    else if (Bug_State.direction == 2)
    {
       STAT = 'B';
    }
    else if (Bug_State.direction == 3)
    {
      STAT = 'L';
    }
    else if (Bug_State.direction == 4)
    {
      STAT = 'R';
    }
    delay(1);
}

//Creating the subscriber 
ros::Subscriber<robot_system::Joy_2_Arduino> sub("Arduino", &messageCb, 1);
ros::Publisher IR_Sen("Ard_Sensor", &ir_msg);

// Creating the motors 
Motor L_Motor(13, 8, 11, A1);
Motor R_Motor(12, 9, 3, A0);

void setup() {

pinMode(7, INPUT);
pinMode(5, INPUT);
pinMode(4, INPUT);
pinMode(10, INPUT);
pinMode(A5, INPUT);

// Initiate the node and subscribe
nh.initNode();
nh.subscribe(sub);
nh.advertise(IR_Sen);
}


void loop() 
{
  read_sensors();
//  read_Motors();
  *PtrState = 'S';          
  IR_Sen.publish( &ir_msg );
  nh.spinOnce(); //call the Node 
  Obs_Avoid();
  StateChange();
  delay(5);
}

//------------------------------------------------------------------------

//  State change using serial

void StateChange()

{
  //------ G = Full speed ahead---------------

  if (*PtrState == 'G')

  {
    FullSpeed();
  }

  //-------  L = turn left --------------------

  else if (*PtrState == 'L')

  {

    TurnLeft();

  }

  //-------  R = turn Right --------------------

  else if (*PtrState == 'R')

  {
    TurnRight();
  }

//-------  B = Back --------------------

  else if (*PtrState == 'B')

  {
    Reverse();
  }
//------------ S = STOP for Anything Else --------------------
  else
  {
    Stop();
  }

}
//----------- DRIVING FUNCTIONS ----------------
//------- TURN RIGHT --------------------
void TurnLeft()
{
    R_Motor.Brake_OFF();
    L_Motor.Brake_OFF();
    L_Motor.Drive(0, Speed);
    R_Motor.Drive(1, Speed); 
}

//-------- TURN LEFT --------------------
void  TurnRight()

{
    R_Motor.Brake_OFF();
    L_Motor.Brake_OFF();
    L_Motor.Drive(1, Speed);
    R_Motor.Drive(0, Speed); 
}
//----------------- FULL SPEED ----------------
void FullSpeed()
{
    R_Motor.Brake_OFF();
    L_Motor.Brake_OFF();
    L_Motor.Drive(1, Speed);
    R_Motor.Drive(1, Speed);
}
//------------------ STOP ---------------------
void Stop()
{
    L_Motor.Drive(1, 0);
    R_Motor.Drive(1, 0);
    R_Motor.Brake_ON();
    L_Motor.Brake_ON();
}
//---------------- REVERSE ---------------------
void Reverse()
{
    R_Motor.Brake_OFF();
    L_Motor.Brake_OFF();
    L_Motor.Drive(0, Speed);
    R_Motor.Drive(0, Speed);
}

// Read The sensors
void read_sensors()
{
  ir_msg.left_ir = digitalRead(7);
  ir_msg.right_ir = digitalRead(10);
  ir_msg.front_left_ir = digitalRead(5);
  ir_msg.front_middle_ir = digitalRead(4);
  ir_msg.front_right_ir = digitalRead(A5);
  ir_msg.front_bottom_left_ir = digitalRead(A4);
  ir_msg.front_bottom_right_ir = digitalRead(A3);
  ir_msg.rear_bottom_left_ir = digitalRead(2);
  ir_msg.rear_bottom_right_ir = digitalRead(6);
  ir_msg.bug_mot_L = L_Motor.Sense();
  ir_msg.bug_mot_R = R_Motor.Sense();
}

//This function will make the buggy avoid objects 
void Obs_Avoid()
{
  if (digitalRead(7) == 0 || digitalRead(5) == 0)
    {
      STAT = 'L';
    }
  
  else if (digitalRead(10) == 0 || digitalRead(A5) == 0)
    {
      STAT = 'R';
    }
    
  if (digitalRead(4) == 0)
    {
      STAT = 'B';
    }
}


