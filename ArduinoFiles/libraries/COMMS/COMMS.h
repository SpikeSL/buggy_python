/* -----------------------------------------------------------------------------
					By Lahiru Dinalankara AKA Spike
							Date 2017/02/10
		
		This Library is designed to Communicate on the serial port.
		
				Functions :
					 Send(EncoderInfo)
					 Read() -> Reads the character sent and places it in
							   a variable and sends a pointer out
		
---------------------------------------------------------------------------------*/

#ifndef COMMS
#define COMMS

#include "Arduino.h"


class Comms
{
	public:
		Comms();
		
		void Send(int Encoder);
		char* Read();
		char STATE;
		
	private:



};

#endif
