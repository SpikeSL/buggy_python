/* -----------------------------------------------------------------------------
					By Lahiru Dinalankara AKA Spike
							Date 2017/02/10
		
		This Library is designed to Communicate on the serial port.
		
				Functions :
					 Send(EncoderInfo)
					 Read() -> Reads the character sent and places it in
							   a variable and sends a pointer out
		
---------------------------------------------------------------------------------*/

#include "Arduino.h"
#include "COMMS.h"

Comms::Comms()
{
}

// This  reads the serial and return the state pointer
char* Comms::Read()
{


  if (Serial.available() > 0)
  {
    STATE = Serial.read();
  }  

//-------- This would make the buggy stop if there is no command to move foward
//  else
//	STATE = 'S';
	
return(&STATE);
}

// This will send the encoder data on the serial
void Comms::Send(int Encoder)
{
	Serial.println(Encoder);
}



