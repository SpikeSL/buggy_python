#ifndef _ROS_robot_system_Joy_2_buggy_h
#define _ROS_robot_system_Joy_2_buggy_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace robot_system
{

  class Joy_2_buggy : public ros::Msg
  {
    public:
      typedef int8_t _pitch_type;
      _pitch_type pitch;
      typedef int8_t _tilt_type;
      _tilt_type tilt;
      typedef bool _headlights_type;
      _headlights_type headlights;
      typedef bool _cam_reset_type;
      _cam_reset_type cam_reset;

    Joy_2_buggy():
      pitch(0),
      tilt(0),
      headlights(0),
      cam_reset(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int8_t real;
        uint8_t base;
      } u_pitch;
      u_pitch.real = this->pitch;
      *(outbuffer + offset + 0) = (u_pitch.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->pitch);
      union {
        int8_t real;
        uint8_t base;
      } u_tilt;
      u_tilt.real = this->tilt;
      *(outbuffer + offset + 0) = (u_tilt.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->tilt);
      union {
        bool real;
        uint8_t base;
      } u_headlights;
      u_headlights.real = this->headlights;
      *(outbuffer + offset + 0) = (u_headlights.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->headlights);
      union {
        bool real;
        uint8_t base;
      } u_cam_reset;
      u_cam_reset.real = this->cam_reset;
      *(outbuffer + offset + 0) = (u_cam_reset.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->cam_reset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int8_t real;
        uint8_t base;
      } u_pitch;
      u_pitch.base = 0;
      u_pitch.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->pitch = u_pitch.real;
      offset += sizeof(this->pitch);
      union {
        int8_t real;
        uint8_t base;
      } u_tilt;
      u_tilt.base = 0;
      u_tilt.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->tilt = u_tilt.real;
      offset += sizeof(this->tilt);
      union {
        bool real;
        uint8_t base;
      } u_headlights;
      u_headlights.base = 0;
      u_headlights.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->headlights = u_headlights.real;
      offset += sizeof(this->headlights);
      union {
        bool real;
        uint8_t base;
      } u_cam_reset;
      u_cam_reset.base = 0;
      u_cam_reset.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->cam_reset = u_cam_reset.real;
      offset += sizeof(this->cam_reset);
     return offset;
    }

    const char * getType(){ return "robot_system/Joy_2_buggy"; };
    const char * getMD5(){ return "0b98d20005bd68d5a4b42d00dfc7ad47"; };

  };

}
#endif