#ifndef _ROS_robot_system_cpu_h
#define _ROS_robot_system_cpu_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace robot_system
{

  class cpu : public ros::Msg
  {
    public:
      typedef uint8_t _cpu_usage_type;
      _cpu_usage_type cpu_usage;
      typedef uint8_t _cpu_mem_type;
      _cpu_mem_type cpu_mem;
      typedef uint8_t _cpu_temp_type;
      _cpu_temp_type cpu_temp;
      typedef uint16_t _b_sent_type;
      _b_sent_type b_sent;
      typedef uint16_t _b_recv_type;
      _b_recv_type b_recv;

    cpu():
      cpu_usage(0),
      cpu_mem(0),
      cpu_temp(0),
      b_sent(0),
      b_recv(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->cpu_usage >> (8 * 0)) & 0xFF;
      offset += sizeof(this->cpu_usage);
      *(outbuffer + offset + 0) = (this->cpu_mem >> (8 * 0)) & 0xFF;
      offset += sizeof(this->cpu_mem);
      *(outbuffer + offset + 0) = (this->cpu_temp >> (8 * 0)) & 0xFF;
      offset += sizeof(this->cpu_temp);
      *(outbuffer + offset + 0) = (this->b_sent >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->b_sent >> (8 * 1)) & 0xFF;
      offset += sizeof(this->b_sent);
      *(outbuffer + offset + 0) = (this->b_recv >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->b_recv >> (8 * 1)) & 0xFF;
      offset += sizeof(this->b_recv);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      this->cpu_usage =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->cpu_usage);
      this->cpu_mem =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->cpu_mem);
      this->cpu_temp =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->cpu_temp);
      this->b_sent =  ((uint16_t) (*(inbuffer + offset)));
      this->b_sent |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->b_sent);
      this->b_recv =  ((uint16_t) (*(inbuffer + offset)));
      this->b_recv |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->b_recv);
     return offset;
    }

    const char * getType(){ return "robot_system/cpu"; };
    const char * getMD5(){ return "19956bee324aa2cdfa9c1eb9c529facb"; };

  };

}
#endif