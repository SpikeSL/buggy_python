#ifndef _ROS_robot_system_Joy_2_Arduino_h
#define _ROS_robot_system_Joy_2_Arduino_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace robot_system
{

  class Joy_2_Arduino : public ros::Msg
  {
    public:
      typedef uint8_t _direction_type;
      _direction_type direction;
      typedef uint8_t _speed_type;
      _speed_type speed;

    Joy_2_Arduino():
      direction(0),
      speed(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->direction >> (8 * 0)) & 0xFF;
      offset += sizeof(this->direction);
      *(outbuffer + offset + 0) = (this->speed >> (8 * 0)) & 0xFF;
      offset += sizeof(this->speed);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      this->direction =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->direction);
      this->speed =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->speed);
     return offset;
    }

    const char * getType(){ return "robot_system/Joy_2_Arduino"; };
    const char * getMD5(){ return "72e6ac43ff44f3e9b14121225e85f2ed"; };

  };

}
#endif