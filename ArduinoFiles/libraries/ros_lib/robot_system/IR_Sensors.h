#ifndef _ROS_robot_system_IR_Sensors_h
#define _ROS_robot_system_IR_Sensors_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace robot_system
{

  class IR_Sensors : public ros::Msg
  {
    public:
      typedef bool _left_ir_type;
      _left_ir_type left_ir;
      typedef bool _right_ir_type;
      _right_ir_type right_ir;
      typedef bool _front_left_ir_type;
      _front_left_ir_type front_left_ir;
      typedef bool _front_middle_ir_type;
      _front_middle_ir_type front_middle_ir;
      typedef bool _front_right_ir_type;
      _front_right_ir_type front_right_ir;
      typedef bool _front_bottom_left_ir_type;
      _front_bottom_left_ir_type front_bottom_left_ir;
      typedef bool _front_bottom_right_ir_type;
      _front_bottom_right_ir_type front_bottom_right_ir;
      typedef bool _rear_bottom_left_ir_type;
      _rear_bottom_left_ir_type rear_bottom_left_ir;
      typedef bool _rear_bottom_right_ir_type;
      _rear_bottom_right_ir_type rear_bottom_right_ir;
      typedef uint8_t _bug_mot_L_type;
      _bug_mot_L_type bug_mot_L;
      typedef uint8_t _bug_mot_R_type;
      _bug_mot_R_type bug_mot_R;

    IR_Sensors():
      left_ir(0),
      right_ir(0),
      front_left_ir(0),
      front_middle_ir(0),
      front_right_ir(0),
      front_bottom_left_ir(0),
      front_bottom_right_ir(0),
      rear_bottom_left_ir(0),
      rear_bottom_right_ir(0),
      bug_mot_L(0),
      bug_mot_R(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_left_ir;
      u_left_ir.real = this->left_ir;
      *(outbuffer + offset + 0) = (u_left_ir.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->left_ir);
      union {
        bool real;
        uint8_t base;
      } u_right_ir;
      u_right_ir.real = this->right_ir;
      *(outbuffer + offset + 0) = (u_right_ir.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->right_ir);
      union {
        bool real;
        uint8_t base;
      } u_front_left_ir;
      u_front_left_ir.real = this->front_left_ir;
      *(outbuffer + offset + 0) = (u_front_left_ir.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->front_left_ir);
      union {
        bool real;
        uint8_t base;
      } u_front_middle_ir;
      u_front_middle_ir.real = this->front_middle_ir;
      *(outbuffer + offset + 0) = (u_front_middle_ir.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->front_middle_ir);
      union {
        bool real;
        uint8_t base;
      } u_front_right_ir;
      u_front_right_ir.real = this->front_right_ir;
      *(outbuffer + offset + 0) = (u_front_right_ir.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->front_right_ir);
      union {
        bool real;
        uint8_t base;
      } u_front_bottom_left_ir;
      u_front_bottom_left_ir.real = this->front_bottom_left_ir;
      *(outbuffer + offset + 0) = (u_front_bottom_left_ir.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->front_bottom_left_ir);
      union {
        bool real;
        uint8_t base;
      } u_front_bottom_right_ir;
      u_front_bottom_right_ir.real = this->front_bottom_right_ir;
      *(outbuffer + offset + 0) = (u_front_bottom_right_ir.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->front_bottom_right_ir);
      union {
        bool real;
        uint8_t base;
      } u_rear_bottom_left_ir;
      u_rear_bottom_left_ir.real = this->rear_bottom_left_ir;
      *(outbuffer + offset + 0) = (u_rear_bottom_left_ir.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->rear_bottom_left_ir);
      union {
        bool real;
        uint8_t base;
      } u_rear_bottom_right_ir;
      u_rear_bottom_right_ir.real = this->rear_bottom_right_ir;
      *(outbuffer + offset + 0) = (u_rear_bottom_right_ir.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->rear_bottom_right_ir);
      *(outbuffer + offset + 0) = (this->bug_mot_L >> (8 * 0)) & 0xFF;
      offset += sizeof(this->bug_mot_L);
      *(outbuffer + offset + 0) = (this->bug_mot_R >> (8 * 0)) & 0xFF;
      offset += sizeof(this->bug_mot_R);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_left_ir;
      u_left_ir.base = 0;
      u_left_ir.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->left_ir = u_left_ir.real;
      offset += sizeof(this->left_ir);
      union {
        bool real;
        uint8_t base;
      } u_right_ir;
      u_right_ir.base = 0;
      u_right_ir.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->right_ir = u_right_ir.real;
      offset += sizeof(this->right_ir);
      union {
        bool real;
        uint8_t base;
      } u_front_left_ir;
      u_front_left_ir.base = 0;
      u_front_left_ir.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->front_left_ir = u_front_left_ir.real;
      offset += sizeof(this->front_left_ir);
      union {
        bool real;
        uint8_t base;
      } u_front_middle_ir;
      u_front_middle_ir.base = 0;
      u_front_middle_ir.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->front_middle_ir = u_front_middle_ir.real;
      offset += sizeof(this->front_middle_ir);
      union {
        bool real;
        uint8_t base;
      } u_front_right_ir;
      u_front_right_ir.base = 0;
      u_front_right_ir.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->front_right_ir = u_front_right_ir.real;
      offset += sizeof(this->front_right_ir);
      union {
        bool real;
        uint8_t base;
      } u_front_bottom_left_ir;
      u_front_bottom_left_ir.base = 0;
      u_front_bottom_left_ir.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->front_bottom_left_ir = u_front_bottom_left_ir.real;
      offset += sizeof(this->front_bottom_left_ir);
      union {
        bool real;
        uint8_t base;
      } u_front_bottom_right_ir;
      u_front_bottom_right_ir.base = 0;
      u_front_bottom_right_ir.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->front_bottom_right_ir = u_front_bottom_right_ir.real;
      offset += sizeof(this->front_bottom_right_ir);
      union {
        bool real;
        uint8_t base;
      } u_rear_bottom_left_ir;
      u_rear_bottom_left_ir.base = 0;
      u_rear_bottom_left_ir.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->rear_bottom_left_ir = u_rear_bottom_left_ir.real;
      offset += sizeof(this->rear_bottom_left_ir);
      union {
        bool real;
        uint8_t base;
      } u_rear_bottom_right_ir;
      u_rear_bottom_right_ir.base = 0;
      u_rear_bottom_right_ir.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->rear_bottom_right_ir = u_rear_bottom_right_ir.real;
      offset += sizeof(this->rear_bottom_right_ir);
      this->bug_mot_L =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->bug_mot_L);
      this->bug_mot_R =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->bug_mot_R);
     return offset;
    }

    const char * getType(){ return "robot_system/IR_Sensors"; };
    const char * getMD5(){ return "989c8bc32f5e5d3236872faf3c1a27b4"; };

  };

}
#endif