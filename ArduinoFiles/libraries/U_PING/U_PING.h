/* -----------------------------------------------------------------------------
					By Lahiru Dinalankara AKA Spike
							Date 2017/02/10
		
		This library is designed to determine the distance to an object.
		when creating an object, the user need to enter the trigger pin, echo 
		pin and the threshold. The threshold is the time to wait until a 
		response from the echo pin. if there is no reply the function will return 
		a 0
---------------------------------------------------------------------------------*/



#ifndef U_PING
#define U_PING

#include "Arduino.h"

class B_Ping
{
		public:
			//Assign the Pins | TRIG | ECHO |  THRESHOLD
			B_Ping(int TRIG, int ECHO, int THRESH);
			int* BugPing();
			//Time from the sensor
			int Time;

		//Private variables	
		private:
			int _Threshold;
			void Pulse();
			int _TRIG;
			int _ECHO;
};

#endif
