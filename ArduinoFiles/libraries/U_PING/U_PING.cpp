/* -----------------------------------------------------------------------------
					By Lahiru Dinalankara AKA Spike
							Date 2017/02/10
		
		This library is designed to determine the distance to an object.
		when creating an object, the user need to enter the trigger pin, echo 
		pin and the threshold. The threshold is the time to wait until a 
		response from the echo pin. if there is no reply the function will return 
		a 0 
---------------------------------------------------------------------------------*/


#include "Arduino.h"
#include "U_PING.h"

//Create the class
//Assign the Pins | TRIG | ECHO LEFT | ECO RIGHT
//Set the pin mode

B_Ping::B_Ping(int TRIG, int ECHO, int THRESH)
{
	
	_TRIG = TRIG;
	_ECHO = ECHO;
	
	pinMode(_TRIG, OUTPUT);
	pinMode(_ECHO, INPUT);
	_Threshold = THRESH;

}

// This function sends a pulse to the trigger pin
void B_Ping::Pulse ()
{
	digitalWrite(_TRIG, LOW);
	delayMicroseconds(2);
	digitalWrite(_TRIG, HIGH);
	delayMicroseconds(5);
	digitalWrite(_TRIG, LOW);
}

int* B_Ping::BugPing()
{
	//Send a pulse to Trigger
	Pulse();	
	Time = pulseIn(_ECHO, HIGH, _Threshold);
	
	//This returns the pointer to the Time
	return(&Time);
	
}
