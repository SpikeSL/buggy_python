/* -----------------------------------------------------------------------------
					By Lahiru Dinalankara AKA Spike
							Date 2017/02/10
		
		This Library is designed to control the buggy Motors.
		When the object is created
		it will need (DIRECTION, BREAK, SPEED, CURRENT SENSING)
				Functions :
					 Drive(DIRECTION, SPEED)
					 BREAK ON
					 BREAK OFF
					 TELL_POWER SENSE(POINTER TO A VARIABLE)
		PWM A -> Pin - 3
		PWM B -> Pin - 11
		Brake A ->  Pin - 9
		Brake B -> 	Pin - 8
		Dir A ->  Pin 12
		Dir B ->  Pin 13
		POWER A  -> A0
		POWER B  -> A1
---------------------------------------------------------------------------------*/

#ifndef MOTOR_CONTROL
#define MOTOR_CONTROL

#include "Arduino.h"


class Motor
{
	public:
		Motor(int DIRECTION, int BREAK, int SPEED, char CURR_SENSE);
		
		void Drive(int _DIR, int _SPEED);
		void Brake_ON();
		void Brake_OFF();
		int* Sense();
		int Curr;
		
		
	private:
		int _DIR_PIN;
		int _BRK_PIN;
		int _SPEED_PIN;
		char _CURR_PIN;
		
		int _DIR;
		int _SPEED;

};

#endif
