/* -----------------------------------------------------------------------------
					By Lahiru Dinalankara AKA Spike
							Date 2017/02/10
		
		This Library is designed to control the buggy Motors.
		When the object is created
		it will need (DIRECTION, BREAK, SPEED, CURRENT SENSING)
				Functions :
					 Drive(DIRECTION, SPEED)
					 BREAK ON
					 BREAK OFF
					 TELL_POWER SENSE(POINTER TO A VARIABLE)
		PWM A -> Pin - 3
		PWM B -> Pin - 11
		Brake A ->  Pin - 9
		Brake B -> 	Pin - 8
		Dir A ->  Pin 12
		Dir B ->  Pin 13
		POWER A  -> A0
		POWER B  -> A1
---------------------------------------------------------------------------------*/

#include "Arduino.h"
#include "BUG_MOTOR.h"


// Initialising the pins
//setting the pinmode

Motor::Motor(int DIRECTION, int BREAK, int SPEED, char CURR_SENSE)
{
	//Assign the pins
	_DIR_PIN = DIRECTION;
	_BRK_PIN = BREAK;
	_SPEED_PIN = SPEED;
	_CURR_PIN = CURR_SENSE;
	
	
	//Call the PINMODE Functions
	pinMode(_DIR_PIN, OUTPUT);
	pinMode(_BRK_PIN, OUTPUT);
	pinMode(_SPEED_PIN, OUTPUT);
	pinMode(_CURR_PIN, INPUT);
	

}

// Function to drive the motors

void Motor::Drive(int _DIR, int _SPEED)
{
	digitalWrite(_DIR_PIN, _DIR);
	analogWrite(_SPEED_PIN, _SPEED );
}

// Function for Brake
void Motor::Brake_ON()
{
	digitalWrite(_BRK_PIN, HIGH);
}

void Motor::Brake_OFF()
{
	digitalWrite(_BRK_PIN, LOW);
}

int* Motor::Sense()
{
	Curr = analogRead(_CURR_PIN);
	return(&Curr);
}
