#!/usr/bin/env python
# =============================================================================================================================================================
#                                                               THE HEADS UP DISPLAY
#                                           SHOW THE VIDEO STREAM FROM THE ROBOT AND THE SENSOR DATA
#                                                          DESIGNED BY LAHIRU DINALANKARA
# =============================================================================================================================================================

import roslib
import rospy
import tf.transformations
from robot_system.msg import IR_Sensors
from sensor_msgs.msg import CompressedImage
from robot_system.msg import cpu
from cv_bridge import CvBridge, CvBridgeError
import cv2
import numpy as np


# ============================================================================================================================================================


class PlotScreen:
    def __init__(self):
        self.bridge = CvBridge()                                                           # Creating the CV Bridge
        self.new_image = cv2.imread('/home/spike/catkin_ws/src/robot_system/src/HUD.png')  # Background for the HUD requres the Ablute path to work
        self.ir_msg = IR_Sensors()                                                         # IR sensor object
        self.CPU = cpu()                                                                   # Create cpu object

        self.x_offset = 400                                                             # Offset of the video window
        self.y_offset = 800 - 650
        self.cpu_information = ['No Info', 'No Info', 'No Info', 'No Info', 'No Info']  # incase disconnected
        self.pi_info_y = 715

        # ------------------- COLORS ------------------------------------

        self.RED = [0, 0, 255]
        self.GREEN = [0, 255, 0]
        self.BLUE = [255, 0, 0]
        self.WHITE = [255, 255, 255]
        self.BLACK = [0, 0, 0]

        # -------------------- LIST STORING THE POSTION FOR THE PRi VAL POSITIONS ------------------

        self.RPi_list = ((380, self.pi_info_y),
                         (500, self.pi_info_y),
                         (640, self.pi_info_y),
                         (780, self.pi_info_y),
                         (910, self.pi_info_y),)

        # -------------------- LIST STORING THE POSTION FOR THE SENSORS ------------------

        self.ir_list = ((90, 55),  # front left prox Sensor
                        (80, 110),  # left prox Sensor
                        (150, 55),  # front middle prox Sensor#
                        (215, 55),  # front right prox Sensor#
                        (235, 110),  # right prox Sensor#
                        (100, 470),  # front bottom left Prox Sensor#
                        (210, 470),  # front bottom right Prox Sensor
                        (100, 700),  # rear bottom left Prox Sensor
                        (210, 700),  # rear bottom right Prox Sensor
                        (450, 300))  # rear centre prox sensor

    # ------------------------------------------ Take the video stream from the message and merge it with the HUD background ----------------------------------------
    def callbackimg(self, data):
        self.video_stream = self.bridge.compressed_imgmsg_to_cv2(data, 'passthrough')       # The video message uncompressed to Numpy array
        self.new_image[self.y_offset:self.y_offset + self.video_stream.shape[0], self.x_offset:self.x_offset + self.video_stream.shape[1]] = self.video_stream  # image from video placed in the HUD numpy array
        cv2.imshow('Data', self.new_image)                                                  # Show the final image
        cv2.waitKey(3)

# ------------------------------------------------------ Reads the values from the sensors and places in a list -------------------------------------------------
    def callback(self, ir):
        self.sensor_values = [ir.front_left_ir, ir.left_ir, ir.front_middle_ir, ir.front_right_ir, ir.right_ir, ir.front_bottom_left_ir, ir.front_bottom_right_ir, ir.rear_bottom_left_ir, ir.rear_bottom_right_ir, 0]
        self.Show_HUD(self.sensor_values)


# ------------------------------------------ Appends the sensor values to the HUD background depending if they are active or not --------------------------------
    def Show_HUD(self, senseor_values):
        for val in range(0, 9):                                                             # Take the sensor data and using the values in ir_list plot them in the HUD
            if senseor_values[val] == False:
                cv2.rectangle(self.new_image, (self.ir_list[val][0]-3, self.ir_list[val][1]-3 ), (self.ir_list[val][0]+3, self.ir_list[val][1]+3 ), self.RED, -2, 8, 0)     # plot a red box
            else:
                cv2.rectangle(self.new_image, (self.ir_list[val][0]-3, self.ir_list[val][1]-3 ), (self.ir_list[val][0]+3, self.ir_list[val][1]+3 ), self.GREEN, -2, 8, 0)    # plot a green box
        cv2.rectangle(self.new_image, (372, 700), (1065, 730), self.BLACK, -2, 8, 0)        # draw a rectangle to erase the old numbers

        for pos in range(0, 5):                                                             # plot the RPi sensor values
            cv2.putText(self.new_image, str(self.cpu_information[pos]), self.RPi_list[pos], cv2.FONT_HERSHEY_DUPLEX, .5,(255, 255, 255))

    def plot_cpu(self, cpu_info):
        self.cpu_information[0] = 'CPU use:' + str(cpu_info.cpu_usage) + '%'
        self.cpu_information[1] = 'Memory:' + str(cpu_info.cpu_mem) + '%'
        self.cpu_information[2] = 'CPU Temp:' + str(cpu_info.cpu_temp) + 'C'
        self.cpu_information[3] = 'Sent:' + str(cpu_info.b_sent) + 'Mbps'
        self.cpu_information[4] = 'Received:' + str(cpu_info.b_recv) + 'Mbps'


# --------------------------------------------------------- LISTNER - listen to the sensor values and image topic -----------------------------------------------
def listener():
    rospy.init_node('HUD', anonymous=True)                                                   # Init node
    rate = rospy.Rate(20)                                                                    # refresh rate
    rospy.Subscriber("Buggy/Ard_Sensor", IR_Sensors, hud.callback, queue_size=1)             # The sensors subscriber
    rospy.Subscriber("Buggy/image_topic_2", CompressedImage, hud.callbackimg, queue_size=1)  # video subscriber
    rospy.Subscriber("Buggy/CPU_INFO", cpu, hud.plot_cpu, queue_size=1)                      # cpu info subscriber
    rate.sleep()                                                                             # pause to see the screen
    rospy.spin()                                                                             # repeat

# ==============================================================================================================================================================

if __name__ == '__main__':
    hud = PlotScreen()
    listener()
else:
    cv2.destroyAllWindows()                                                         # destroy windows after closing
