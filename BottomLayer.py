#!/usr/bin/env python

from time import sleep
import roslib
import rospy
import tf.transformations
from std_msgs.msg import Int8
from robot_system.msg import IR_Sensors
from robot_system.msg import Joy_2_Arduino
ir_msg = IR_Sensors()

direction = 0
speed = 0
j2a = Joy_2_Arduino()


def callback(ir):
    global direction
    global speed
    j2a.speed = speed
    if not ir.left_ir or not ir.front_left_ir:
        for x in range (0, 25):
            direction = 3
            talker()
            
    if not ir.right_ir or not ir.front_right_ir:
        for x in range (0, 25):
            direction = 4
            talker()
            
    if not ir.front_middle_ir:
        direction = 2
    else:
        j2a.direction = direction
    
    talker()


def turn_right():
    pass

    
def joystick(joy):
    global direction
    global speed

    direction = joy.direction
    speed = joy.speed

    
def talker():
    Publish_Arduino = rospy.Publisher('Arduino', Joy_2_Arduino, queue_size=1)
    rospy.loginfo(j2a)
    Publish_Arduino.publish(j2a)

def listener():
    rospy.init_node('BottomLayer', anonymous=True)
    rospy.Subscriber("GPIO_STATS", IR_Sensors, callback)
    rospy.Subscriber("2BotLayer", Joy_2_Arduino, joystick)
    rospy.spin()

if __name__ == '__main__':
    listener()
