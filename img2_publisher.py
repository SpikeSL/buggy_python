#!/usr/bin/env python

# =============================================================================================================================================================
#                                                                   THE VIDEO PUBLISHER
#                                           PUBLISH THE VIDEO STREAM CAPTURED USING OPENCV AND COMPRESSED USING CV BRIDGE
#                                                              DESIGNED BY LAHIRU DINALANKARA
# =============================================================================================================================================================

import roslib
import rospy
import tf.transformations
from sensor_msgs.msg import CompressedImage
import cv2
from cv_bridge import CvBridge, CvBridgeError

class Camera:
    def __init__(self):
        self.cam = cv2.VideoCapture(0)                                                               # create a camera object
        self.bridge = CvBridge()                                                                     # create a bridge object

    # ---------------------------------------------------- define talker to publish the video stream -----------------------------------------------------------
    def talker(self):
        rospy.init_node('Speed', anonymous=True)                                            # init node
        rate = rospy.Rate(20)                                                               # the publish rate
        img_publisher = rospy.Publisher("camera", CompressedImage, queue_size=1)            # publisher to publish images
        while not rospy.is_shutdown():
            _, img = self.cam.read()                                                             # grab the image in camera object
            conv_img = self.bridge.cv2_to_compressed_imgmsg(img, dst_format='jpg')               # compress the grabbed image
            img_publisher.publish(conv_img)                                                 # publish the image stream
            rate.sleep()                                                                    # pause


# ============================================================================================================================================================
try:
    CamPublish = Camera()
    CamPublish.talker()

except KeyboardInterrupt:
    CamPublish.cam.release()



