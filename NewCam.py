
from time import sleep
import roslib
import rospy
import tf.transformations
from sensor_msgs.msg import CompressedImage
from std_msgs.msg import Int8
import cv2
import numpy as np
from cv_bridge import CvBridge, CvBridgeError

cam = cv2.VideoCapture(0)
#cam.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
#cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

# --------------------- DEFINE ARDUINO TALKER----------

bridge = CvBridge()


def talker():
    global cam
    #img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # conv_img = bridge.cv2_to_imgmsg(img, 'bgr8')
    #conv_img = bridge.cv2_to_imgmsg(img, 'mono8')

    print 'Image converted'

    rospy.init_node('Speed', anonymous=True)
    rate = rospy.Rate(50)
    rospy.loginfo('Node Init')

    img_publisher = rospy.Publisher("camera", CompressedImage, queue_size=5)
    print 'publisher init..'
    while not rospy.is_shutdown():
        print 'running'
        _, img = cam.read()
        print img.shape

        conv_img = bridge.cv2_to_compressed_imgmsg(img, dst_format='jpg')
        print conv_img
        img_publisher.publish(conv_img)
#        rate.sleep()


try:
    talker()

except KeyboardInterrupt:
    cap.release()
    cv2.destroyAllWindows()


