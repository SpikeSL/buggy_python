#!/usr/bin/env python
# =============================================================================================================================================================
#                                                           NODE TO CONVERT PS4 JOY TO ARDUINO
#                                     THIS SUBSCRIBES TO THE JOY NODE, READ THE VALUES AND CONVERTS THEM TO VALUES FOR BUGGY
#                                                             DESIGNED BY LAHIRU DINALANKARA
# =============================================================================================================================================================

import roslib
import rospy
from sensor_msgs.msg import Joy
from robot_system.msg import Joy_2_buggy                                    # Custom message for the raspberry pi
from robot_system.msg import Joy_2_Arduino                                  # Custom message for the Arduino
from scipy.interpolate import interp1d                                      # need to remap the values

j2b = Joy_2_buggy()                                                         # custom messages to the buggy
j2a = Joy_2_Arduino()                                                       # custom messages just for arduino -> motors
m = interp1d([0,1],[100,255])                                               # converts the controller values to 100-255

# ===============================================================================================================================================================
# --------------------------------------- TAKE THE VALUES FROM THE JOY NODE AND CONVERTS TO VALUES ARDUINO UNDERSTAND -----------------------------------------

def callback(msg):
    j2a.speed = 255                                                         # The value is set to full speed for fast turning
    j2a.direction = 0                                                       # set the values to zero so the robot won't move when buttons are released
    j2b.pitch = 0                                                           # set the values to zero so the Gimbe won't move randomly
    j2b.tilt = 0                                                            # set the values to zero so the Gimbe won't move randomly

# ---------------- FORWARD REVERSE ---------------------
    if msg.axes[5] > 0.0:
        j2a.direction = 1
        j2a.speed = m((msg.axes[5]))                                        # converts the values |0 - 1| in to values between |100 - 255| for Arduino
    elif msg.axes[5] < 0.0:
        j2a.direction = 2
        j2a.speed = m(msg.axes[5] * -1)                                     # converts the values |0 - 1| in to values between |100 - 255| for Arduino
        
# ---------------- LEFT / RIGHT ------------------------
    if msg.axes[2] > 0.0:
        j2a.direction = 4
        #talker()
    elif msg.axes[2] < 0.0:
        j2a.direction = 3

    Arduino_talker()                                                        # the speed and direction is published to the arduino
# ----------------- PAN  -------------------------------
    if msg.axes[0] < 0:
        j2b.pitch = int(msg.axes[0] * 5)
    elif msg.axes[0] > 0:
        j2b.pitch = int(msg.axes[0] * 5)

# ----------------- TILT -------------------------------
    if msg.axes[1] > 0:
        j2b.tilt = int(msg.axes[1] * 5)
    elif msg.axes[1] < 0:
        j2b.tilt = int(msg.axes[1] * 5)

# ---------------- RESET CAM ---------------------------
    if msg.buttons[10] == 1:
        j2b.cam_reset = 1
    else:
        j2b.cam_reset = 0                                                             

# ----------------- HEADLIGHTS ------------------------
    if msg.buttons[13] ==1:
        j2b.headlights = 1
    else:
        j2b.headlights = 0
    talker()                                                                # publish the messages to the Raspberry Pi
    
# ================================================================================================================================================================
# ------------------------------------------------------------------- DEFINE TALKER ------------------------------------------------------------------------------

def talker():
    Publish_joy = rospy.Publisher('Buggy', Joy_2_buggy, queue_size=5)               # create a publisher for raspberry pi, servo, lights
    rospy.loginfo(j2b)
    Publish_joy.publish(j2b)
    
# --------------------------------------------------------------- DEFINE ARDUINO TALKER --------------------------------------------------------------------------

def Arduino_talker():
    Publish_Ard = rospy.Publisher('Arduino', Joy_2_Arduino, queue_size=3)           # This publisher is targeted to Arduino only
    rospy.loginfo(j2a)
    Publish_Ard.publish(j2a)

# ------------------------------------------------------------ DEFINE PS4 CONTROLLER LISTNER ---------------------------------------------------------------------
def listener():
    rospy.init_node('Joy_2_Buggy', anonymous=True)                                  # create the subscriber to listen to the controller
    rospy.Subscriber("joy", Joy, callback)                                          # run the callback function --> run the arduino talker or buggy talker
    rospy.spin()
    
# ----------------------------------------------------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    listener()
    
