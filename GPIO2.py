#!/usr/bin/env python

from time import sleep
import roslib
import rospy
import RPi.GPIO as GPIO
from robot_system.msg import IR_Sensors


GPIO.setmode(GPIO.BCM)

GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(5, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(6, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(19, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

global GPIO_stat
GPIO_stat = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

ir_msg = IR_Sensors()
# ---------------------CATCH SENSOR VALUES ----------------------------------
def get_GPIO():
    ir_msg.front_left_ir = GPIO.input(4)    # front left prox Sensor
    ir_msg.left_ir = GPIO.input(17)   # left prox Sensor
    ir_msg.front_middle_ir = GPIO.input(27)   # front middle prox Sensor
    ir_msg.front_right_ir = GPIO.input(22)   # front right prox Sensor
    ir_msg.right_ir = GPIO.input(5)    # right prox Sensor
    ir_msg.front_bottom_left_ir = GPIO.input(6)    # front bottom left Prox Sensor
    ir_msg.front_bottom_right_ir = GPIO.input(13)   # front bottom right Prox Sensor
    ir_msg.rear_bottom_left_ir = GPIO.input(19)   # rear bottom left Prox Sensor
    ir_msg.rear_bottom_right_ir = GPIO.input(21)   # rear bottom right Prox Sensor
    
# --------------------- DEFINE GPIO TALKER----------
def talker():    
    publish_GPIO = rospy.Publisher('/GPIO_STATS', IR_Sensors, queue_size=3)
    rospy.init_node('GPIO_Publisher', anonymous=True)
    print 'GPIO_Publisher initialised'
    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
        get_GPIO()
        publish_GPIO.publish(ir_msg)
        rate.sleep()




if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass


