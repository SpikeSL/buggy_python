#!/usr/bin/env python
# =============================================================================================================================================================
#                                                             NODE TO SEND THE CPU USAGE OF THE PI
#                                                   THIS NODE SENDS THE DETAILS OF CPU USAGE TO THE BUGGY USING PSUUTIL
#                                                              DESIGNED BY LAHIRU DINALANKARA
# =============================================================================================================================================================

import roslib
import rospy
import tf.transformations
from robot_system.msg import cpu
import random
import psutil
from subprocess import PIPE, Popen

# =============================================================================================================================================================
# -------------------------------------------------------- store data in cpu object ---------------------------------------------------------------------------
class GetCPU:
    def __init__(self):
        self.CPU = cpu()  # create the cpu object
        self.b_sent_now = psutil.net_io_counters().bytes_sent  # asign sent data to variable
        self.b_recv_now = psutil.net_io_counters().bytes_recv  # assign recv data to variable
    def get_cpu(self):
        self.process = Popen(['vcgencmd', 'measure_temp'], stdout=PIPE)                  # to read the temp of cpu ONLY REQUIRED IN RASPBERRY PI
        self.output, _error = self.process.communicate()                                      # read the temp output and error ONLY REQUIRED IN RASPBERRY PI
        self.CPU.cpu_usage = int(psutil.cpu_percent(interval=None, percpu=False))        # cpu usage percent
        self.CPU.cpu_mem = int(psutil.virtual_memory().percent)                          # memorry usage percent
        self.CPU.cpu_temp = int(float(self.output[self.output.index('=') + 1:self.output.rindex("'")])) # take the cpu temp and convert to float -> int ONLY REQUIRED IN RASPBERRY PI
        self.CPU.b_sent = int((psutil.net_io_counters().bytes_sent - self.b_sent_now) /500)   # sub now sent data from last sent data and make it per sec
        self.CPU.b_recv = int((psutil.net_io_counters().bytes_recv - self.b_recv_now) /500)   # sub now recv data from last recv data and make it per sec
        self.b_sent_now = psutil.net_io_counters().bytes_sent                            # add now sent data to variable
        self.b_recv_now = psutil.net_io_counters().bytes_recv                            # add now recv data to variable


# --------------------------------------------------------------- define talker --------------------------------------------------------------------------------
def talker():
    rospy.init_node('CPU_Usage', anonymous = True)                              # init node
    rate = rospy.Rate(2)                                                        # refresh rate
    cpu_publish = rospy.Publisher("CPU_INFO", cpu, queue_size=1)                # create publisher
    while not rospy.is_shutdown():                                              # publish loop
        getCPU.get_cpu()                                                               # run get cpu
        cpu_publish.publish(getCPU.CPU)
        rate.sleep()

# ==============================================================================================================================================================
try:
    getCPU = GetCPU()
    talker()

except KeyboardInterrupt:
    pass        



