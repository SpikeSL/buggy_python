#!/usr/bin/env python
# THIS NODE WILL TAKE INPUTS FROM PS4 JOY NODE AND PUBLISH IT FOR SERIAL
# PORT TO THE ARDUINO
import roslib
import rospy
from sensor_msgs.msg import Joy
from robot_system.msg import Joy_2_buggy
from robot_system.msg import Joy_2_Arduino
from scipy.interpolate import interp1d



j2b = Joy_2_buggy()     # custom messages to the buggy
j2a = Joy_2_Arduino()   # custom messages just for arduino -> motors
m = interp1d([0,1],[100,255]) # converts the controller values to 100-255

def callback(msg):
    j2a.speed = 255
    j2a.direction = 0
    j2b.pitch = 0
    j2b.tilt = 0
    # ---------------- FORWARD REVERSE ----------
    if msg.axes[5] > 0.0:
        j2a.direction = 1
        j2a.speed = m((msg.axes[5]))
    elif msg.axes[5] < 0.0:
        j2a.direction = 2
        j2a.speed = m(msg.axes[5] * -1)
        
    #Arduino_talker()
    # ---------------- LEFT / RIGHT -------------
    elif msg.axes[2] > 0.0:
        j2a.direction = 4
        #talker()
    elif msg.axes[2] < 0.0:
        j2a.direction = 3

    Arduino_talker()
    # ----------------- PAN  -----------------
    if msg.axes[0] < 0:
        j2b.pitch = int(msg.axes[0] * 5)
    elif msg.axes[0] > 0:
        j2b.pitch = int(msg.axes[0] * 5)

    # ----------------- TILT --------------
    if msg.axes[1] < 0:
        j2b.tilt = int(msg.axes[1] * 5)
    elif msg.axes[1] > 0:
        j2b.tilt = int(msg.axes[1] * 5)

    # ---------------- RESET CAM ----------
    if msg.buttons[10] == 1:
        j2b.cam_reset = 1
    else:
        j2b.cam_reset = 0
    talker()
    

# --------------------- DEFINE TALKER ----------

def talker():
    Publish_joy = rospy.Publisher('Buggy', Joy_2_buggy, queue_size=5)
    rospy.loginfo(j2b)
    Publish_joy.publish(j2b)
    
# --------------------- DEFINE ARDUINO TALKER ----------

def Arduino_talker():
    Publish_Ard = rospy.Publisher('2BotLayer', Joy_2_Arduino, queue_size=3)
    rospy.loginfo(j2a)
    Publish_Ard.publish(j2a)

# --------------------- DEFINE PS4 CONTROLLER LISTNER -----
def listener():
    rospy.init_node('Speed', anonymous=True)
    rospy.Subscriber("joy", Joy, callback)
    rospy.spin()
    

if __name__ == '__main__':
    listener()
    

