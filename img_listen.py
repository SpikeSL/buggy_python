from time import sleep
import roslib
import rospy
import tf.transformations
from sensor_msgs.msg import Image
from std_msgs.msg import Int8
import cv2
import numpy as np
from cv_bridge import CvBridge, CvBridgeError

bridge = CvBridge()


def callback(data):
    cv_image = bridge.imgmsg_to_cv2(data, "bgr8")
    cv2.imshow('Data', cv_image)
    cv2.waitKey(3)


def listner():
    rospy.Subscriber("image_topic_2", Image, callback)
    print('subscriber created')

    rospy.init_node('Speed', anonymous=True)
    print 'node init'

if __name__ == '__main__':
    try:
        listner()
        rospy.spin()
    except KeyboardInterrupt:
        cap.release()
        cv2.destroyAllWindows()