#!/usr/bin/env python
# =============================================================================================================================================================
#                                                             NODE TO CONTROL THE SERVO GIMBLE
#                          THIS NODE SUBSCRIBE TO THE BUGGY NODE AND CONTROL THE SERVOS USING TWO PWM SIGNALS AND PIGPIO LIBRARY
#                                                              DESIGNED BY LAHIRU DINALANKARA
# =============================================================================================================================================================


import roslib
import rospy
import tf.transformations
from robot_system.msg import Joy_2_buggy
import wiringpi
import pigpio


# ============================================================================================================================================================
# ------------------------------------------------------ CREATE SERVO CLASS ----------------------------------------------------------------------------------
class Servo:
    def __init__(self):
        self.pi = pigpio.pi()  # create the GPIO object
        self.j2b = Joy_2_buggy()  # create the buggy messenger
        self.pitch = 1300  # starting pitch
        self.tilt = 1500  # starting tilt
# ------------------------------------------- Call back to increment or decrement servo position -------------------------------------------------------------

    def callback(self, msg):
        if self.pitch > 504 and self.pitch < 2496:
            self.pitch = msg.pitch + self.pitch
        if self.tilt > 1300 and self.tilt < 2496:
            self.tilt = msg.tilt + self.tilt
        if msg.cam_reset is True:                                         # reset the values of pitch and tilt
            self.pitch = 1300
            self.tilt = 1500
        Servo.run_servos()                                                      # run servos for the values to be sent to the servos

    # --------------------------------------------------- Energise the servos with pitch and tilt values ---------------------------------------------------------
    def run_servos(self):
        self.pi.set_servo_pulsewidth(16, self.pitch)
        self.pi.set_servo_pulsewidth(20, self.tilt)

    # ------------------------------------------------------- DeEnergise the servos using zeros -----------------------------------------------------------------
    def reset_servos(self):
        self.pi.set_servo_pulsewidth(16, 0)
        self.pi.set_servo_pulsewidth(20, 0)
        Servo.run_servos()
# ------------------------------------------------------------------------ Listner -------------------------------------------------------------------------- 
def listener():
    Servo.reset_servos()
    rospy.init_node('Servos', anonymous=True)                        # Init node
    rate = rospy.Rate(50)                                            # Rate the system publish
    rospy.Subscriber("Buggy", Joy_2_buggy, Servo.callback, queue_size=1)   # subscribe to the Buggy topic
    rospy.spin()
    rate.sleep()                                                     # pausing to give servos time to move to position


if __name__ == '__main__':
    try:
        Servo = Servo()
        listener()

    except KeyboardInterrupt:
        Servo.reset_servos()                                                 # de energise the servos before exiting the program







