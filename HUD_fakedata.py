#!/usr/bin/env python

import roslib
import rospy
import tf.transformations
from robot_system.msg import IR_Sensors
import random

global vals

ir_msg = IR_Sensors()

def get_GPIO():
    vals = []
    for x in range(0, 10):

        vals.append(random.randint(0, 1))
    ir_msg.left_ir = vals[0]
    ir_msg.right_ir = vals[1]
    ir_msg.front_left_ir = vals[2]
    ir_msg.front_middle_ir = vals[3]
    ir_msg.front_right_ir = vals[4]
    ir_msg.front_bottom_left_ir = vals[5]
    ir_msg.front_bottom_right_ir = vals[6]
    ir_msg.rear_bottom_left_ir = vals[7]
    ir_msg.rear_bottom_right_ir = vals[8]
    print vals
    #return (vals)

def talker():
    
    publish_GPIO = rospy.Publisher('Ard_Sensor', IR_Sensors, queue_size=3)
    rospy.init_node('GPIO_Publisher', anonymous=True)
    print 'GPIO_Publisher initialised'
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        get_GPIO()
        publish_GPIO.publish(ir_msg)
        rate.sleep()




if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass

