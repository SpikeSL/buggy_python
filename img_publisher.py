
from time import sleep
import roslib
import rospy
import tf.transformations
from sensor_msgs.msg import Image
from std_msgs.msg import Int8
import cv2
import numpy as np
from cv_bridge import CvBridge, CvBridgeError

cam = cv2.VideoCapture(0)

# --------------------- DEFINE ARDUINO TALKER----------

bridge = CvBridge()


def talker():
    _, img = cam.read()
    #img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # conv_img = bridge.cv2_to_imgmsg(img, 'bgr8')
    conv_img = bridge.cv2_to_compressed_imgmsg(img, dst_format='jp2')
    #conv_img = bridge.cv2_to_imgmsg(img, 'mono8')

    print 'Image converted'

    rospy.init_node('Speed', anonymous=True)
    rospy.loginfo('Node Init')

    img_publisher = rospy.Publisher("image_topic_2", Image, queue_size=10)
    print 'publisher init..'

    img_publisher.publish(conv_img)


try:
    while True:
        talker()
        sleep(1 / 20)
except KeyboardInterrupt:
    cap.release()
    cv2.destroyAllWindows()
