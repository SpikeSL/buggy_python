import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while True:
    _,frame=cap.read()
    frame=cv2.GaussianBlur(frame,(5,5),0)
    hsv=cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    HSVLOW=np.array([0,0,240])
    HSVHIGH=np.array([162,17,255])

    mask = cv2.inRange(hsv,HSVLOW, HSVHIGH)
    res = cv2.bitwise_and(frame,frame, mask =mask)

    edges = cv2.Canny(res,100,200)
    cv2.imshow('EDGES', edges)

    circles = cv2.HoughCircles(res, cv2.HOUGH_GRADIENT, 1.2, 10)

    if circles is not None:
	# convert the (x, y) coordinates and radius of the circles to integers
        circles = np.round(circles[0, :]).astype("int")
        print circles
	# loop over the (x, y) coordinates and radius of the circles
	for (x, y, r) in circles:
            # draw the circle in the output image, then draw a rectangle
            # corresponding to the center of the circle
            cv2.circle(res, (x, y), r, (255, 255, 255), 4)
            cv2.rectangle(res, (x - 5, y - 5), (x + 5, y + 5), (255, 255, 255), -1)
 
    cv2.imshow('IMAGE',res) 
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()

