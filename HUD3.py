#!/usr/bin/env python

from time import sleep
import roslib
import rospy
import tf.transformations
from std_msgs.msg import Int8
from robot_system.msg import IR_Sensors

import cv2
import numpy as np

   # (520, 150)
img = cv2.imread('HUD.png')
print 'Image read'

ir_msg = IR_Sensors()

ir_list = ((390, 56),   # front left prox Sensor
           (370, 150),  # left prox Sensor
           (450, 56),   # front middle prox Sensor#
           (500, 56),   # front right prox Sensor#
           (520, 150),  # right prox Sensor#
           (330, 620),  # front bottom left Prox Sensor#
           (190, 620),  # front bottom right Prox Sensor
           (560, 620),  # rear bottom left Prox Sensor
           (700, 620),  # rear bottom right Prox Sensor
           (450, 300))  # rear centre prox sensor


def callback(ir):
    print ir.front_left_ir
    sensor_values = [ir.front_left_ir, ir.left_ir, ir.front_middle_ir, ir.front_right_ir, ir.right_ir, ir.front_bottom_left_ir, ir.front_bottom_right_ir, ir.rear_bottom_left_ir, ir.rear_bottom_right_ir, 0]
    print 'Sensor Values from the node %s' %sensor_values
    Show_HUD(sensor_values)


def Show_HUD(senseor_values):
    new_image = img
    for val in range(0, 10):
        if senseor_values[val] == False:
            cv2.circle(new_image, ir_list[val], 6, [0, 0, 255], -2, 8, 0)
        else:
            cv2.circle(new_image, ir_list[val], 6, [0, 0, 0], -2, 8, 0)
    cv2.imshow('HUD', new_image)
    cv2.waitKey(1)

def listener():
    
    sensor_values = ' '
    rospy.init_node('HUD', anonymous=True)
    rate = rospy.Rate(100)
    rospy.Subscriber("Ard_Sensor", IR_Sensors, callback)
    rate.sleep()
    rospy.spin()

if __name__ == '__main__':
    listener()
else:
    cv2.destroyAllWindows()
