import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while True:
    _,frame=cap.read()
    frame=cv2.GaussianBlur(frame,(9,9),0)
    hsv=cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    HSVLOW=np.array([0,0,250])
    HSVHIGH=np.array([0,0,255])

    mask = cv2.inRange(hsv,HSVLOW, HSVHIGH)
    res = cv2.bitwise_and(frame,frame, mask =mask)
    
    cv2.imshow('image', res)
    cv2.imshow('yay', frame)
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break


cv2.destroyAllWindows()
